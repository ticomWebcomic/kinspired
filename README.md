# KINspired

¿Recuerdan el Microsoft KIN?, ese telefono que Microsoft lanzo en el 2010 y que descontinuo en el 2010, ¿No?,
pues hagamos un pequeño ejercicio de nostalgia recordando (o conociendo por primera vez) al KIN y un pequeño
ejercicio de CSS replicando la interfaz grafica

Rehacer esto bien, sacar provecho de CSS Grid

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

Aquí hablaremos de que fue el KIN

### ← main.html

Aquí va una replica de la interfaz de KIN Studio

### ← tmp.txt

Almacen temporal de las referencias

### ← style.css

CSS files add styling rules to your content.

### ← script.js

If you're feeling fancy you can add interactivity to your site with JavaScript.

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
